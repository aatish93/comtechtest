import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule } from "@angular/forms";
import { MeetingRoomComponent } from './meeting-room/meeting-room.component';
import { UiControlComponent } from './ui-control/ui-control.component';
import { UiRegulatorComponent } from './ui-regulator/ui-regulator.component';

@NgModule({
  declarations: [
    AppComponent,
    MeetingRoomComponent,
    UiControlComponent,
    UiRegulatorComponent
  ],
  imports: [
    BrowserModule,FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
