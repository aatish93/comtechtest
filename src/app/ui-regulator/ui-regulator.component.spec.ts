import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiRegulatorComponent } from './ui-regulator.component';

describe('UiRegulatorComponent', () => {
  let component: UiRegulatorComponent;
  let fixture: ComponentFixture<UiRegulatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiRegulatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiRegulatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
