import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-meeting-room',
  templateUrl: './meeting-room.component.html',
  styleUrls: ['./meeting-room.component.scss']
})
export class MeetingRoomComponent implements OnInit {
  public img='assets/like.png';
  public uiControls=[
    {'key':'Phone','icon':'fa fa-mobile','selected':true},
    {'key':'Display','icon':'fa fa-television','selected':false},
    {'key':'Sound','icon':'fa fa-volume-up','selected':false},
    {'key':'Camera','icon':'fa fa-camera','selected':false},
    {'key':'Music','icon':'fa fa-headphones','selected':false},
    {'key':'Silent','icon':'fa fa-phone','selected':false},
    {'key':'Flight','icon':'fa fa-plane','selected':false}
  ];
  public angle=360;
  constructor() { }

  ngOnInit(): void {
  }
  angleChange(event){
    //console.log('angleChange',event);
  }
  afterAngleChange(event){
    //console.log('afterAngleChange',event);
  }
  clickComponent(event,control){
    this.uiControls.map(data=>{
      if(data.key==control.key) data.selected=true;
      else data.selected=false;
    });
  }
}
